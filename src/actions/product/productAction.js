import * as actionTypes from "../../const/types";

export const fetchProduct = () => {
  return {
    type: actionTypes.FETCH_PRODUCTS,
  };
};

export const fetchProductSuccess = (products) => {
  return {
    type: actionTypes.FETCH_PRODUCTS_SUCCESS,
    payload: products,
  };
};
export const fetchProductReject = () => {
  return {
    type: actionTypes.FETCH_PRODUCTS_REJECTED,
  };
};

export const searchForProducts = (field) => {
  let desiredProducts = JSON.parse(localStorage.getItem("productList")).filter(
    product =>
      product.sku.toLowerCase().includes(field.toLowerCase()) ||
      product.name.toLowerCase().includes(field.toLowerCase())
  );
  localStorage.setItem("desiredProducts" , JSON.stringify(desiredProducts));
  return {
    type: actionTypes.SEARCH_FOR_PRODUCTS,
    payload: desiredProducts,
  };
};

export const fetchCurrency = () => {	
  return {
    type: actionTypes.FETCH_CURRENCY,
  };
};

export const fetchCurrencySuccess = (symbol) => {
  return {
    type: actionTypes.FETCH_CURRENCY_SUCCESS,
    payload: symbol,
  };
};
