import * as actionTypes from "../../const/types";

export const addToCart = (cartItems, product) => {
  const cart = cartItems.slice();
  let alreadyInCart = false;
  cart.forEach((item) => {
    if (item.count === undefined) item.count = 0;
    if (product.id === item.id) {
      alreadyInCart = true;
      item.count++;
    }
  });

  if (!alreadyInCart) {
    cart.push({ ...product, count: 1 });
  }
  localStorage.setItem("cartItems", JSON.stringify(cart));
  return {
    type: actionTypes.ADD_TO_CART,
    payload: cart,
  };
};

export const removeFromCart = (cartItems, product) => {
  const cart = cartItems.slice().filter((item) => item.id !== product.id);
  /***
   * save all items in cart to localStorage
   */
  localStorage.setItem("cartItems", JSON.stringify(cart));
  return {
    type: actionTypes.REMOVE_FROM_CART,
    payload: cart,
  };
};

export const openPlaceorderModal = (value) => {
  return {
    type: actionTypes.OPEN_PLACE_ORDER_MODAL,
    payload: value,
  };
};

export const placeorder = () => {
  return {
    type: actionTypes.PLACE_ORDER,
  };
};
export const placeorderSuccess = (orderId) => {
  return {
    type: actionTypes.PLACE_ORDER_SUCCESS,
    payload: orderId,
  };
};
export const placeorderError = () => {
  return {
    type: actionTypes.PLACE_ORDER_ERROR,
  };
};
