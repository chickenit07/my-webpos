import * as actionTypes from "../../const/types";

export const setListCustomerModalOpen = (value) => {

  return {
	type: actionTypes.SET_LIST_CUS_OPEN,
	payload: value
  };
};
	
export const setNewCustomerModalOpen = (value) => {
  return {
	type: actionTypes.SET_NEW_CUS_OPEN,
	payload: value
  };
};

export const addCustomerToCart = (customer) => {
	return {
		type: actionTypes.ADD_CUSTOMER_TO_CART,
		payload: customer
	}
}
