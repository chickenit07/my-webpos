import * as actionTypes from '../const/types'

export const getLoginInfo = (info) => {
	// debugger
	return {
		type: actionTypes.FETCH_ACCOUNT_INFO,
		payload: info
	}
}

export const getLoginSuccess = () => {
	return {
		type: actionTypes.FETCH_ACCOUNT_INFO_SUCCESS,
	}
}