import React from 'react'

export default function Logo() {
	return (
		<div>
			<img style={{
				width: '30%'
			}} 
			src={"https://uploads-ssl.webflow.com/5e74408bb8e83144e441090b/5e746914d815914007873e9d_Magestore_Logo_RGB_Yellow-Black.png"} alt="Logo" />;
		</div>
	)
}
