import React from "react";
import ".././style/menu.css";
export default function LeftMenu() {
  return (
    <div className="left-menu">
      <ul>
        <li>
          <a className="active" href="home">
            Checkout
          </a>
        </li>
        <li>
          <a href="#news">Settings</a>
        </li>
        <li>
          <a href="#contact">Order</a>
        </li>
        <li>
          <a href="#about">Log out</a>
        </li>
      </ul>
    </div>
  );
}
