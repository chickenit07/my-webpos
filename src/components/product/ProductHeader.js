import React, { useState } from "react";
import "../../style/product.css";
import { connect } from "react-redux";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { searchForProducts } from "../../actions/product/productAction";
const mapStateToProps = (state) => {
  return {
    ...state.products,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSearchForProducts: (field) => {
      dispatch(searchForProducts(field));
    },
  };
};
function ProductHeader(props) {
  const [field, setField] = useState("");

  const onInputChange = (event) => {
    setField(event.target.value);
  };

  const onSubmit = (e) => {
    e.preventDefault(); // prevent redirect to another page
	props.onSearchForProducts(field);
  };

  return (
    <Form onSubmit={onSubmit} className="product-header">
      <Row>
        <Col>
          <Form.Control  placeholder="Search by Sku or Name" onChange={onInputChange} />
        </Col>
        <button className="btn" type="submit">
			Search
		</button>
      </Row>
    </Form>
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductHeader);
