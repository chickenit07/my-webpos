import React, { Component } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBCardFooter,
} from "mdbreact";
import { formatMediaUrl, formatSku } from "../../const/utils";
import "../../style/product.css";
import { connect } from "react-redux";
import { baseUrl } from "../../const/common";
import { addToCart } from "../../actions/cart/cartAction";

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToCart: (cartItems, product) => {
      dispatch(addToCart(cartItems, product));
    },
  };
};
const mapStateToProps = (state) => {
  return {
    cartItems: state.cart.cartItems,
  };
};
class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avail: 0,
    };
  }

  componentDidMount() {
    fetch(baseUrl + "getStockQty/" + this.props.productId, {
      method: "GET",
      headers: {
        Authorization: localStorage.getItem("token"),
        "content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          avail: data,
        });
      });
  }

  render() {
    return (
      <MDBCard
        onClick={() =>
          this.props.onAddToCart(this.props.cartItems, this.props.info)
        }
      >
        <MDBCardImage
          cascade
          top
          src={formatMediaUrl(this.props.info.image)}
          waves
        />
        <MDBCardBody cascade className="text-center">
          <MDBCardTitle>{this.props.info.name}</MDBCardTitle>
          <MDBCardText>
            SKU: {formatSku(this.props.info.sku)} - Price: {this.props.currency}
            {this.props.info.price}
          </MDBCardText>
          <MDBCardText></MDBCardText>
          <MDBCardFooter>
            <MDBCardText>
              Avail: {this.state.avail} - Type: {this.props.info.type}
            </MDBCardText>
          </MDBCardFooter>
        </MDBCardBody>
      </MDBCard>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);
