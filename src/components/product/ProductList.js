import React, { Component } from "react";
import { connect } from "react-redux";
import Product from "./Product";
import "../../style/product.css";

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};
class ProductList extends Component {
  render() {
	  const { currency } = this.props.products;
    let products = JSON.parse(localStorage.getItem("desiredProducts"));
    if (!products) products = JSON.parse(localStorage.getItem("productList"));
    return (
      <div>
        <div className="product-list">
          {products.map((product) => {
            return (
              <div key={product.id}>
                <Product
                  productId={product.id}
                  currency={currency}
                  info={product}
                  className="product"
                ></Product>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(ProductList);
