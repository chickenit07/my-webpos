import React, { Component } from "react";
import { Col, Row } from "react-bootstrap";
import "../../style/cart.css";
import CartItem from "./CartItem";
import { connect } from "react-redux";
import "../../style/cart.css";
import Button from "react-bootstrap/Button";
import CustomerCart from "../customer/CustomerCart";
import * as cartAction from "../../actions/cart/cartAction";
import PlaceOrderModal from "./PlaceOrderModal";
const mapDispatchToProps = (dispatch) => {
  return {
    onOpenPlaceorderModal: (value) => {
      dispatch(cartAction.openPlaceorderModal(value));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cart.cartItems,
    currency: state.products.currency,
  };
};

class Cart extends Component {
  render() {
    /***
     * get cartItem from localStorage
     */
    let cartItems = JSON.parse(localStorage.getItem("cartItems") || "[]");
    let total = cartItems.reduce(
      (sum, item) => sum + item.count * item.price,
      0
    );

    return (
      <div className="cart-box">
        <Row>
          <Col className="customer-box">
            <CustomerCart />
          </Col>
        </Row>
        <Row>
          <Col className="cart-item">
            {cartItems.map((item) => (
              <li key={item.id}>
                <CartItem info={item} />
              </li>
            ))}
          </Col>
        </Row>
        <Row className="cart-footer">
          <Button
            variant="primary"
            disabled={!cartItems.length}
            size="lg"
            onClick={() => {
              this.props.onOpenPlaceorderModal(true);
            }}
          >
            {this.props.currency}
            {total}
          </Button>
		  <PlaceOrderModal total={total}/>
        </Row>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
