import React from "react";
import Modal from "react-bootstrap/Modal";
import { Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { connect } from "react-redux";
import * as cartAction from "../../actions/cart/cartAction";

const mapDispatchToProps = (dispatch) => {
  return {
    onSetModalPlaceOrderOpen: (value) => {
      dispatch(cartAction.openPlaceorderModal(value));
	},
	onPlaceorder: () => {
		dispatch(cartAction.placeorder());
	}
  };
};
const mapStateToProps = (state) => {
  return {
    cart: state.cart,
    products: state.products,
  };
};

function PlaceOrderModal(props) {
  const placeorder = () => {
    props.onPlaceorder();
  };
  const hideModal = () => {
    props.onSetModalPlaceOrderOpen(false);
  };

  const { isModalPlaceOrderOpen } = props.cart;
  const { currency } = props.products;
  return (
    <Modal
      backdrop="static"
      keyboard={false}
      show={isModalPlaceOrderOpen}
      onHide={() => hideModal()}
    >
      <Modal.Header>
        <Row>PLACE ORDER</Row>
      </Modal.Header>
      <Modal.Body>
        <Row className="payment-box">
          <img
            src="https://image.freepik.com/free-vector/cash-money-icon-design_1692-69.jpg"
            alt="cash"
          ></img>
        </Row>
        <Row>
          Total: {currency} {props.total}
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button className="btn btn-primary" onClick={() => hideModal()}>
          Cancel
        </Button>
        <Button className="btn btn-primary" onClick={() => placeorder()}>
          Place order
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceOrderModal);
