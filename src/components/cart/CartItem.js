import React from "react";
import { Col, Row } from "react-bootstrap";
import { MEDIA_PATH } from "../../const/common";
import { connect } from "react-redux";
import { removeFromCart } from "../../actions/cart/cartAction";

const mapDispatchToProps = (dispatch) => {
  return {
    onRemoveFromCart: (cartItems, item) => {
      dispatch(removeFromCart(cartItems, item));
    },
  };
};
const mapStateToProps = (state) => {
  return {
    cartItems: state.cart.cartItems,
    currency: state.products.currency,
  };
};
function CartItem(props) {
  return (
    <Row className="item">
      <Col>
        <img src={MEDIA_PATH + props.info.image} alt={props.info.id}></img>
      </Col>
      <Col xs={7}>
        {props.info.name} x <strong>{props.info.count}</strong>
      </Col>
      <Col className="price-info-item" sm={2}>
        {props.info.price * props.info.count} {props.currency}
      </Col>
      <Col className="close-item" sm={1}>
        <button
          onClick={() => props.onRemoveFromCart(props.cartItems, props.info)}
        >
          X
        </button>
      </Col>
    </Row>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(CartItem);
