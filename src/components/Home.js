import React from "react";
import { connect } from "react-redux";
import CartHeader from "./cart/CartHeader";
import Cart from "./cart/Cart";
import ProductHeader from "./product/ProductHeader";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import ProductList from "./product/ProductList";
import Loading from "./Loading"
import '../style/home.css';

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};
function Home(props) {
  const { isLoadingConfig } = props.products;
	
  if (!isLoadingConfig)
    return (
      <div id="root-home">
        <Container className="con-left">
          <Row>
              <CartHeader/>
              <Cart />
          </Row>
        </Container>
        <Container className="con-right">
          <Row>
              <ProductHeader/>
              <ProductList />
          </Row>
        </Container>
      </div>
    );
  else {
    return (
     <Loading>
	 </Loading>
    );
  }
}

export default connect(mapStateToProps, null)(Home);
