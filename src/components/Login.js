import { connect } from "react-redux";
import "../style/login.css";
import "antd/dist/antd.css";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import * as loginActions from "../actions/loginActions";
import Logo from "./common/Logo";

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchingLoginInfo: (info) => {
      dispatch(loginActions.getLoginInfo(info));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    handleLogin: state.isAuthStaff,
  };
};

function Login(props) {
    return (
      <div className="center-component">
        <Logo></Logo>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={(value) => {
            props.onFetchingLoginInfo(value);
          }}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Please input your Username!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
