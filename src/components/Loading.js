import React, { Component } from "react";
import { connect } from "react-redux";
import * as productAction from ".././actions/product/productAction";
import Spinner from "react-bootstrap/Spinner";

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchingProduct: () => {
      dispatch(productAction.fetchProduct());
    },
    onFetchingCurrency: () => {
      dispatch(productAction.fetchCurrency());
	},
	onFetchedProduct: (productList) => {
		dispatch(productAction.fetchProductSuccess(productList))
	}
  };
};
class Loading extends Component {
  componentDidMount() {
	this.props.onFetchingCurrency();
    const productList = JSON.parse(localStorage.getItem("productList"));
	if (productList) {
		this.props.onFetchedProduct(productList);
	} else this.props.onFetchingProduct()
  }
  render() {
    return (	
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
    );
  }
}
export default connect(null, mapDispatchToProps)(Loading);
