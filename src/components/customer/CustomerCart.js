import React from "react";
import { connect } from "react-redux";
import * as customerAction from "../../actions/customer/customerAction";
import Button from "react-bootstrap/Button";
import CustomerModal from "./CustomerModal";
import '../../style/cart.css';

const mapDispatchToProps = (dispatch) => {
  return {
    onSetListCustomerModalOpen: (value) => {
      dispatch(customerAction.setListCustomerModalOpen(value));
    },
    onSetNewCustomerModalOpen: (value) => {
      dispatch(customerAction.setNewCustomerModalOpen(value));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    customerCart: state.customer,
  };
};

function CustomerCart(props) {
  const {isCustomer} = props.customerCart;
  const showListModal = () => {
    props.onSetListCustomerModalOpen(true);
    // props.onSetNewCustomerModalOpen(false);
  };
  return (
    <div>
      <Button onClick={() => showListModal()}>{isCustomer.firstName}</Button>
      <CustomerModal />
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerCart);
