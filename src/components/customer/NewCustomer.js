import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { connect } from "react-redux";
import * as customerAction from "../../actions/customer/customerAction";

const mapDispatchToProps = (dispatch) => {
  return {
    onSetListCustomerModalOpen: (value) => {
      dispatch(customerAction.setListCustomerModalOpen(value));
    },
    onSetNewCustomerModalOpen: (value) => {
      dispatch(customerAction.setNewCustomerModalOpen(value));
    },
    onAddCustomerToCart: (customer) => {
      dispatch(customerAction.addCustomerToCart(customer));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    customerCart: state.customer,
  };
};

function NewCustomer(props) {
  const hideModalNew = () => {
    props.onSetNewCustomerModalOpen(false);
  };

  const { isNewCustomerModalOpen, isCustomer } = props.customerCart;

  const onSubmit = (e) => {
    e.preventDefault(); //prevent redirect to another page
    const firstName = e.target.formGridFirstName.value;
    const lastName = e.target.formGridLastName.value;
    const email = e.target.formGridEmail.value;
    const phoneNum = e.target.formGridPhoneNum.value;
    const address = e.target.formGridAddress.value;
    const addressDetail = e.target.formGridAddressDetail.value;
    const customer_info = {
      firstName,
      lastName,
      email,
      phoneNum,
      address,
      addressDetail,
    };
	let customerList = localStorage.getItem("customerList" || [isCustomer]);
	/**
	 * bug in here
	 */
	if (!customerList.length) {
      localStorage.setItem("customerList", JSON.stringify([customer_info]));
    } else {
      customerList.forEach((customer) => {
		  if (customer.email !== customer_info.email) {
			return customerList.push(customer_info);
			} else {
				return alert("There is another one using this mail.");  
			}
		});
      props.onSetNewCustomerModalOpen(false);
      props.onAddCustomerToCart(customer_info);
    }
  };
  return (
    <Modal
      backdrop="static"
      keyboard={false}
      show={isNewCustomerModalOpen}
      onHide={hideModalNew}
    >
      <Modal.Header>
        <Modal.Title>Create new customer</Modal.Title>
      </Modal.Header>
      <Form onSubmit={onSubmit}>
        <Modal.Body>
          <Form.Row>
            <Form.Group controlId="formGridFirstName">
              <Form.Label>First name</Form.Label>
              <Form.Control type="text" placeholder="First name" required />
            </Form.Group>
            <Form.Group controlId="formGridLastName">
              <Form.Label>Last name</Form.Label>
              <Form.Control type="text" placeholder="Last name" />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group controlId="formGridEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder="Email" />
            </Form.Group>

            <Form.Group controlId="formGridPhoneNum">
              <Form.Label>Phone</Form.Label>
              <Form.Control type="tel" placeholder="Phone number" required />
            </Form.Group>
          </Form.Row>
          <Form.Group controlId="formGridAddress">
            <Form.Label>Address</Form.Label>
            <Form.Control placeholder="1234 Main St" />
          </Form.Group>

          <Form.Group controlId="formGridAddressDetail">
            <Form.Label>Address Detail</Form.Label>
            <Form.Control placeholder="Apartment, studio, or floor" />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => hideModalNew()}>Cancel</Button>
          <Button variant="primary" type="submit">
            Save
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCustomer);
