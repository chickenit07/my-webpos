import React from "react";
import { Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import * as customerAction from "../../actions/customer/customerAction";

const mapDispatchToProps = (dispatch) => {
  return {
    onAddCustomerToCart: (customer) => {
	  dispatch(customerAction.addCustomerToCart(customer));
	  dispatch(customerAction.setListCustomerModalOpen(false));
	  dispatch(customerAction.setNewCustomerModalOpen(false));
    },
  };
};

function CustomerItem(props) {
  const addCustomerToCart = (customer) => {
    props.onAddCustomerToCart(customer);
  };

  return (
    <Row className="customer-item" onClick={() => addCustomerToCart(props.info)}>
      <Col xs={8}>{props.info.firstName + " " + props.info.lastName}</Col>
      <Col	>
        {props.info.phoneNum}
      </Col>
    </Row>
  );
}
export default connect(null, mapDispatchToProps)(CustomerItem);
