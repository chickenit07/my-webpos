import React from "react";
import { connect } from "react-redux";
import ListCustomer from "./ListCustomer";
import NewCustomer from "./NewCustomer";

const mapStateToProps = (state) => {
  return {
    customerCart: state.customer,
  };
};

function CustomerModal(props) {
  const {
    isNewCustomerModalOpen,
    isListCustomerModalOpen,
  } = props.customerCart;
  if (isListCustomerModalOpen) return <ListCustomer />;
  else if (isNewCustomerModalOpen) return <NewCustomer />;
  else return <div></div>;
}
export default connect(mapStateToProps)(CustomerModal);
