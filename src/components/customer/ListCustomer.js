import React  from 'react'
import Modal from "react-bootstrap/Modal";
import { Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import { connect } from "react-redux";
import CustomerItem from './CustomerItem';
import * as customerAction from "../../actions/customer/customerAction";
import NewCustomer from './NewCustomer';


const mapDispatchToProps = (dispatch) => {
	return {
	  onSetListCustomerModalOpen: (value) => {
		dispatch(customerAction.setListCustomerModalOpen(value));
	  },
	  onSetNewCustomerModalOpen: (value) => {
		dispatch(customerAction.setNewCustomerModalOpen(value));
	  },
	};
  };
const mapStateToProps = state  => {
	return {
		customerCart: state.customer,
	}
}

function ListCustomer(props) {
	const showModalNew =() => {
		props.onSetNewCustomerModalOpen(true);
		hideModalList();
	}
	const hideModalList = () => {
		props.onSetListCustomerModalOpen(false)
	}

	const {
		isListCustomerModalOpen,
	  } = props.customerCart;
	const customerList = JSON.parse(localStorage.getItem("customerList") || "[]");
	// debugger
	return (
		<Modal show={isListCustomerModalOpen} onHide={() => hideModalList()}>
        <Modal.Header>
          <Button className="btn btn-primary" onClick={() => showModalNew()}>
            Create Customer
			<NewCustomer/>
          </Button>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <FormControl type="text" placeholder="Search" className="search" />
          </Form>
          <Row className="list-customer">
			 {customerList.map((customer) => (
				 <li key={customerList.indexOf(customer)} >	
					<CustomerItem info={customer}/>
				 </li>
			 ))}
          </Row>
        </Modal.Body>
      </Modal>
	)
}

export default  connect(mapStateToProps,mapDispatchToProps)(ListCustomer);