import { MEDIA_PATH } from "./common";

export function formatMediaUrl(url){
	return MEDIA_PATH.concat(url);
}

export function formatSku(sku){
	return "[" + sku + ']';
}