export const FETCH_ACCOUNT_INFO = "FETCH_ACCOUNT_INFO"
export const FETCH_ACCOUNT_INFO_SUCCESS = "FETCH_ACCOUNT_INFO_SUCCESS"
export const FETCH_ACCOUNT_INFO_ERROR ="FETCH_ACCOUNT_INFO_ERROR"

export const FETCH_PRODUCTS = "FETCH_PRODUCTS"
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS"
export const FETCH_PRODUCTS_CANCEL = "FETCH_PRODUCTS_CANCEL"
export const FETCH_PRODUCTS_REJECTED = "FETCH_PRODUCTS_REJECTED"

export const SEARCH_FOR_PRODUCTS = "SEARCH_FOR_PRODUCTS"
export const ADD_TO_CART = "ADD_TO_CART"
export const REMOVE_FROM_CART = "REMOVE_FROM_CART"

export const FETCH_CURRENCY = "FETCH_CURRENCY"
export const FETCH_CURRENCY_SUCCESS = "FETCH_CURRENCY_SUCCESS"

export const ADD_CUSTOMER_TO_CART = "ADD_CUSTOMER_TO_CART"
export const REMOVE_CUSTOMER_TO_CART = "REMOVE_CUSTOMER_FROM_CART"
export const SAVE_CUSTOMER = "SAVE_CUSTOMER"
export const SET_LIST_CUS_OPEN = "SET_LIST_CUS_OPEN"
export const SET_NEW_CUS_OPEN = "SET_NEW_CUS_OPEN"
export const CREATE_NEW_CUSTOMER = "CREATE_NEW_CUSTOMER"

export const PLACE_ORDER = "PLACE_ORDER"
export const PLACE_ORDER_SUCCESS="PLACE_ORDER_SUCCESS"
export const PLACE_ORDER_ERROR = "PLACE_ORDER_ERROR"
export const OPEN_PLACE_ORDER_MODAL = "OPEN_PLACE_ORDER_MODAL"