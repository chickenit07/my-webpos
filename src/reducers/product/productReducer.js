import * as actionTypes from "../../const/types";

const initState = {
  products: [],
  isLoadingConfig: true,
  currency: ""
};

export const productReducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_PRODUCTS:
      return {
        ...state,
      };
    case actionTypes.FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload,
        isLoadingConfig: false,
      };
    case actionTypes.FETCH_PRODUCTS_REJECTED:
      return {
        ...state,
      };
    case actionTypes.SEARCH_FOR_PRODUCTS:
      return {
        ...state,
        products: action.payload,
      };
    case actionTypes.FETCH_CURRENCY_SUCCESS:
      return {
        ...state,
        currency: action.payload,
      };
    default:
      return state;
  }
};
