import * as actionTypes from "../../const/types";

const initState = {
  cartItems: [],
  isModalPlaceOrderOpen: false,
};
	
export const cartReducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.ADD_TO_CART:
      return {
        ...state,
        cartItems: action.payload,
      };
    case actionTypes.REMOVE_FROM_CART:
      return {
        ...state,
        cartItems: action.payload,
      };
    case actionTypes.OPEN_PLACE_ORDER_MODAL:
      return {
        ...state,
        isModalPlaceOrderOpen: action.payload,
	  };
	  
    /**
     * needed or not?
     */
    case actionTypes.PLACE_ORDER_SUCCESS:
      let orders = localStorage.getItem("orders" || []);
	  orders.push(action.payload);
      return {
        ...initState,
      };
    default:
      return state;
  }
};
