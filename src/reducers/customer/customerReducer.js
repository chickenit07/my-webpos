import * as actionTypes from "../../const/types";

const initState = {
  isCustomerEmpty: true,
  isCustomer: {
    firstName: "GUEST",
    lastName: "GUEST",
    email: "guest@webpos.com",
    phoneNum: "0000000000",
    address: "test",
  },
  isNewCustomerModalOpen: false,
  isListCustomerModalOpen: false,
};

export const customerReducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.SET_LIST_CUS_OPEN:
      return {
        ...state,
        isListCustomerModalOpen: action.payload,
      };
    case actionTypes.SET_NEW_CUS_OPEN:
      return {
        ...state,
        isNewCustomerModalOpen: action.payload,
      };
    case actionTypes.ADD_CUSTOMER_TO_CART:
      return {
        ...state,
        isCustomerEmpty: false,
        isCustomer: action.payload,
      };
    /**
     * needed or not?
     */
    case actionTypes.PLACE_ORDER_SUCCESS:
      localStorage.setItem("cartItems", []);
      return {
        ...initState,
      };
    default:
      return state;
  }
};
