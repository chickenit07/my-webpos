import {combineReducers} from 'redux'
import {loginReducer} from './loginReducer';
import {productReducer} from './product/productReducer';
import {cartReducer} from './cart/cartReducer';
import {customerReducer} from './customer/customerReducer';

const rootReducer = combineReducers({
	login: loginReducer,
	products : productReducer,
	cart: cartReducer,
	customer: customerReducer
})

export default rootReducer;