import * as actionTypes from "../const/types";

const initState = {
	isLoggedIn : false
};
export const loginReducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_ACCOUNT_INFO:
      return { ...state };
    case actionTypes.FETCH_ACCOUNT_INFO_SUCCESS:
	  localStorage.setItem("token", action.payload.token);
	  localStorage.setItem("isLogged", true);
      return {
		isLoggedIn : true
	  };
    default:
      return state;
  }
};
