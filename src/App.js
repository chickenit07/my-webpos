import Home from "./components/Home";
import Login from "./components/Login";
import React from "react";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.login,
  };
};

function App() {
  const isLoggedIn = localStorage.getItem("isLogged");
  if (isLoggedIn) return <Home/>;
  else return <Login/>;
}

export default connect(mapStateToProps)(App);
