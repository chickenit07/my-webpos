import * as actionTypes from "../const/types";
// import { getLoginSuccess } from "../actions/loginActions";
import { ajax } from "rxjs/ajax";
import { map, mergeMap } from "rxjs/operators";
import { ofType } from "redux-observable";
import { loginPostRequest } from "../const/common";

const fetchLoginEpic = (action$, state$) => {
  return action$.pipe(
    ofType(actionTypes.FETCH_ACCOUNT_INFO),
    mergeMap((action) => {
      const req = {
        url: loginPostRequest,
        method: "POST",
        headers: {
          "content-type": "application/json",
        },
        body: action.payload,
      };
      return ajax(req).pipe(
        map((res) => {
          if (res.response !== null) {
            const staffName = action.payload.username;
			localStorage.setItem("username", staffName);
            return {
              type: actionTypes.FETCH_ACCOUNT_INFO_SUCCESS,
              payload: {
                token: res.response
              },
            };
          } else {
            alert("Invalid username or password!");
            return {
              type: actionTypes.FETCH_ACCOUNT_INFO_ERROR,
            };
          }
        })
      );
    })
  );
};
export default fetchLoginEpic;
