import * as actionTypes from "../../const/types";
import {
  fetchCurrencySuccess
} from "../../actions/product/productAction";
import { ajax } from "rxjs/ajax";
import { map, mergeMap } from "rxjs/operators";
import { ofType } from "redux-observable";
import { baseUrl } from "../../const/common";

const fetchCurrency = (action$, state$) =>
  action$.pipe(
	ofType(actionTypes.FETCH_CURRENCY),
	mergeMap(() =>
	ajax
	  .getJSON(
		baseUrl + "getCurrencySymbol",
		{
		  Authorization: localStorage.getItem("token"),
		  "content-type": "application/json",
		}
	  )
	  .pipe(
		map((rs) => {
			return fetchCurrencySuccess(rs);
		})
	  )
  )
);

export default fetchCurrency;
