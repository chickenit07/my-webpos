import * as actionTypes from "../../const/types";
import {
  fetchProductSuccess,
  fetchProductReject
} from "../../actions/product/productAction";
import { ajax } from "rxjs/ajax";
import { map, mergeMap } from "rxjs/operators";
import { ofType } from "redux-observable";
import { baseUrl } from "../../const/common";

const fetchProductEpic = (action$, state$) =>
  action$.pipe(
    ofType(actionTypes.FETCH_PRODUCTS),
    mergeMap(() =>
      ajax
        .getJSON(
          baseUrl +
            `getListProducts?
			searchCriteria[filter_groups][0][filters][0][field]=visible_on_pos
			&searchCriteria[filter_groups][0][filters][0][condition_type]=eq
			&searchCriteria[filter_groups][0][filters][0][value]=1`,
          {
            Authorization: localStorage.getItem("token"),
            "content-type": "application/json",
          }
        )
        .pipe(
          map((response) => {
            try {
              let rs = response.items.map((product) => ({
                id: product.id,
                sku: product.sku,
                name: product.name,
                price: product.price,
				type: product.type_id,
				image: product.custom_attributes[0].value,
              }));
              /***
               * save all products in database that fulfilled visible_on_pos = 1.
               */
              localStorage.setItem("productList", JSON.stringify(rs));
              return fetchProductSuccess(rs);
            } catch (error) {
              return fetchProductReject();
            }
          })
        )
    )
  )
export default fetchProductEpic;
