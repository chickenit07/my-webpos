import { combineEpics } from "redux-observable";
import fetchLoginEpic from "./loginEpic";
import fetchProductEpic from "./product/productEpics";
import fetchCurrency from "./product/fetchCurrency";
import placeorderEpic from "./placeorderEpic";

const rootEpic = combineEpics(
  fetchLoginEpic,
  fetchProductEpic,
  fetchCurrency,
  placeorderEpic
);
export default rootEpic;
