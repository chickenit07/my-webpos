import * as actionTypes from "../const/types";
// import { getLoginSuccess } from "../actions/loginActions";
import { ajax } from "rxjs/ajax";
import { map, mergeMap } from "rxjs/operators";
import { ofType } from "redux-observable";
import { baseUrl } from "../const/common";
import { placeorderSuccess } from "../actions/cart/cartAction";

const placeorderEpic = (action$, state$) => {
  return action$.pipe(
    ofType(actionTypes.PLACE_ORDER),
    mergeMap(() => {
		const { isCustomer } = state$.value.customer;
		debugger
      const cartItem =  JSON.parse(localStorage.getItem("cartItems")).map((item) => {
		debugger
		return {
          sku: item.sku,
          qty: item.qty,
          price: item.price,
        };
      });
      const req = {
        url: baseUrl + 'webpos/checkout/placeorder',
        method: "POST",
        headers: {
          Authorization: localStorage.getItem("token"),
          "content-type": "application/json",
        },
        body: {
          customer: {
            firstName: isCustomer.firstName,
            lastName: isCustomer.lastName,
            email: isCustomer.email,
          },
          cartItem: cartItem,
          paymentMethod: "cashondelivery",
          addressInformation: {
            shipping_address: {},
            billing_address: {},
          },
        },
      };
      console.log(req);
      return ajax(req).pipe(map((response) => placeorderSuccess(response)));
    })
  );
};
export default placeorderEpic;
